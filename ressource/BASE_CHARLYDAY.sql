
CREATE DATABASE IF NOT EXISTS `BaseDonnee`;

use `BaseDonnee`;

CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `nomUser` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prenomUser` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mdpUser` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `emailUser` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `imageUser` int(11) NOT NULL,
  `pseudoUser` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `messageUser` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

CREATE TABLE IF NOT EXISTS `logement` (
  `idLogement` int(11) NOT NULL AUTO_INCREMENT,
  `disponibilite` int(2) NOT NULL,
  `placesLogement` int(11) NOT NULL,
  `iconeLogement` int(11) NOT NULL,
  `adresseLogement` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idLogement`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

CREATE TABLE IF NOT EXISTS `groupe` (
  `idGroupe` int(11) NOT NULL AUTO_INCREMENT,
  `statutGroupe` int(2) NOT NULL,
  `urlGroupe` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `idUser` int(11) NOT NULL,
  `idLogement` int(11) NOT NULL,
  PRIMARY KEY (`idGroupe`),
  FOREIGN KEY (`idUser`) REFERENCES user(`idUser`),
  FOREIGN KEY (`idLogement`) REFERENCES logement(`idLogement`)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `admin` (
  `idAdmin` int(11) NOT NULL AUTO_INCREMENT,
  `mdpAdmin` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pseudoAdmin` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idAdmin`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

CREATE TABLE IF NOT EXISTS `invitation` (
  `idUser` int(11) NOT NULL,
  `idGroupe` int(11) NOT NULL,
  `statutGroupe` int(2) NOT NULL,
  PRIMARY KEY (`idUser`, `idGroupe`),
  FOREIGN KEY (`idUser`) REFERENCES user(`idUser`),
  FOREIGN KEY (`idGroupe`) REFERENCES groupe(`idGroupe`)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;
CREATE TABLE IF NOT EXISTS `note` (
  `idLogement` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `valeurNote` int(2) NOT NULL,
  PRIMARY KEY (`idUser`, `idLogement`),
  FOREIGN KEY (`idUser`) REFERENCES user(`idUser`),
  FOREIGN KEY (`idLogement`) REFERENCES logement(`idLogement`)


) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;
CREATE TABLE IF NOT EXISTS `photologement` (
  `idPhoto` int(11) NOT NULL,
  `idLogement` int(11) NOT NULL,
  PRIMARY KEY (`idPhoto`),
  FOREIGN KEY (`idLogement`) REFERENCES logement(`idLogement`)


) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

INSERT INTO `logement` (`idLogement`,`disponibilite`,`placesLogement`,`iconeLogement`) VALUES
( 1, 1, 3,  1),
( 2, 1, 3,  2),
( 3, 1, 4,  3),
( 4, 1, 4,  4),
( 5, 1, 4,  5),
( 6, 1, 4,  6),
( 7, 1, 4,  7),
( 8, 1, 5,  8),
( 9, 1, 5,  9),
(10, 1, 6, 10),
(11, 1, 6, 11),
(12, 1, 6, 12),
(13, 1, 7, 13),
(14, 1, 7, 14),
(15, 1, 8, 15),
(16, 1, 2, 16),
(17, 1, 2, 17),
(18, 1, 2, 18),
(19, 1, 2, 19),
(20, 1, 2, 20),
(21, 1, 3, 21),
(22, 1, 3, 22),
(23, 1, 3, 23),
(24, 1, 3, 24),
(25, 1, 3, 25);

INSERT INTO `admin` (`idAdmin`,`mdpAdmin`,`pseudoAdmin`) VALUES
(1, 'adminl', 'adminl');


INSERT INTO `user` (`idUser`, `mdpUser`,`emailUser`,`imageUser`,`nomUser`,`pseudoUser`, `prenomUser`,`messageUser`) VALUES
(1 ,'mdp','a@b.c',1 ,'Mathieu', 'Jeanne',    'Jeanne',    'aime la musique ♫'),
(2 ,'mdp','a@b.c',2 ,'Mathieu', 'Paul',      'Paul',      'aime cuisiner ♨ ♪'),
(3 ,'mdp','a@b.c',3 ,'Mathieu', 'Myriam',    'Myriam',    'mange Halal ☪'),
(4 ,'mdp','a@b.c',4 ,'Mathieu', 'Nicolas',   'Nicolas',   'ouvert à tous ⛄'),
(5 ,'mdp','a@b.c',5 ,'Mathieu', 'Sophie',    'Sophie',    'aime sortir ♛'),
(6 ,'mdp','a@b.c',6 ,'Mathieu', 'Karim',     'Karim',     'aime le soleil ☀'),
(7 ,'mdp','a@b.c',7 ,'Mathieu', 'Julie',     'Julie',     'apprécie le calme ☕'),
(8 ,'mdp','a@b.c',8 ,'Mathieu', 'Etienne',   'Etienne',   'accepte jeunes et vieux ☯'),
(9 ,'mdp','a@b.c',9 ,'Mathieu', 'Max',       'Max',       'féru de musique moderne ☮'),
(10,'mdp','a@b.c',10,'Mathieu', 'Sabrina',   'Sabrina',   'aime les repas en commun ⛵☻'),
(11,'mdp','a@b.c',11,'Mathieu', 'Nathalie',  'Nathalie',  'bricoleuse ⛽'),
(12,'mdp','a@b.c',12,'Mathieu', 'Martin',    'Martin',    'sportif ☘ ⚽ ⚾ ⛳'),
(13,'mdp','a@b.c',13,'Mathieu', 'Manon',     'Manon',     ''),
(14,'mdp','a@b.c',14,'Mathieu', 'Thomas',    'Thomas',    ''),
(15,'mdp','a@b.c',15,'Mathieu', 'Léa',       'Léa',       ''),
(16,'mdp','a@b.c',16,'Mathieu', 'Alexandre', 'Alexandre', ''),
(17,'mdp','a@b.c',17,'Mathieu', 'Camille',   'Camille',   ''),
(18,'mdp','a@b.c',18,'Mathieu', 'Quentin',   'Quentin',   ''),
(19,'mdp','a@b.c',19,'Mathieu', 'Marie',     'Marie',     ''),
(20,'mdp','a@b.c',20,'Mathieu', 'Antoine',   'Antoine',   ''),
(21,'mdp','a@b.c',21,'Mathieu', 'Laura',     'Laura',     ''),
(22,'mdp','a@b.c',22,'Mathieu', 'Julien',    'Julien',    ''),
(23,'mdp','a@b.c',23,'Mathieu', 'Pauline',   'Pauline',   ''),
(24,'mdp','a@b.c',24,'Mathieu', 'Lucas',     'Lucas',     ''),
(25,'mdp','a@b.c',25,'Mathieu', 'Sarah',     'Sarah',     ''),
(26,'mdp','a@b.c',26,'Mathieu', 'Romain',    'Romain',    ''),
(27,'mdp','a@b.c',27,'Mathieu', 'Mathilde',  'Mathilde',  ''),
(28,'mdp','a@b.c',28,'Mathieu', 'Florian',   'Florian',   '');
