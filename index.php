<?php
session_start();
include_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager;
use controleur\Controleur;
use controleur\ControleurUtilisateur;
use controleur\ControleurConnexion;
use controleur\ControleurLogement;
use controleur\ControleurGroupe;
use conf\Eloquent;

\Slim\Slim::registerAutoloader();

Eloquent::init('src/conf/config.ini');

$app = new \Slim\slim();


$app->post('/connect',function(){
    (new ControleurConnexion())->seConnecter();
});
$app->post('/inscription',function(){
    (new ControleurConnexion())->inscription();
});
//Afficher accueil
$app->get('/',function(){
	(new Controleur())->afficherAccueil();
})->name('accueil');

$app->get('/membres/',function(){
	(new ControleurUtilisateur())->utilisateurs();
})->name('membres');

$app->get('/membres/:id',function($id){
	(new ControleurUtilisateur())->utilisateur($id);
});

$app->get('/connexion/',function(){
    (new ControleurConnexion())->connexion();
});
/*
$app->get('/seConnecter(/)',function(){
    (new ControleurConnexion())->seConnecter();
});
*/
$app->post('/seConnecter(/)',function(){
    (new ControleurConnexion())->seConnecter();
});
//$app->get('/)

$app->get('/logements/',function(){
	(new ControleurLogement())->logements();
});

$app->get('/logements/:id',function($id){
	(new ControleurLogement())->logement($id);
});

$app->get('/groupes/', function(){
	(new ControleurGroupe())->groupes();
});

$app->get('/groupes/:id', function(){
	(new ControleurGroupe())->groupe($id);
});





$app->run();
