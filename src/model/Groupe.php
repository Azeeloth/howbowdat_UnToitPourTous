<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace model;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Groupe
 *
 * @author Mathieu
 */
class Groupe extends Model{
    
    protected $table="groupe";
    protected $primaryKey="idGroupe";
    public $timestamps=false;
    
    public function getUser(){
        return $this->hasOne("\model\User","idUser");
    }
    
    public function getInvite(){
        return $this->hasOne("\model\Invitation","idGroupe");
    }
    
    public function getLogement(){
        return $this->belongsTo("\model\Logement","idGroupe");
    }
    
}
