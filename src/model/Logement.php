<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace model;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Logement
 *
 * @author Mathieu
 */
class Logement extends Model{
    
    protected $table="logement";
    protected $primaryKey="idLogement";
    public $timestamps=false;
    
    public function getGroupe(){
        return $this->hasMany("\model\Groupe","idGroupe");
    }
    
    public function getNote(){
        return $this->hasMany("\model\Note", "idLogement");
    }
    
    public function getPhotos(){
        return $this->hasMany("\model\PhotoLogement","idLogement");
    }
    
}
