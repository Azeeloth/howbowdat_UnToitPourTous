<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace model;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of User
 *
 * @author Mathieu
 */
class User extends Model{
    
    protected $table="user";
    protected $primaryKey="idUser";
    public $timestamps=false;
    
    public function getGroupe(){
        return $this->hasOne("\model\Groupe","idGroupe");
    }
    
    public function getInvite(){
        return $this->hasOne("\model\Invitation","idUser");
    }
    
}
