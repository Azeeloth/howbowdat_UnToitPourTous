<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace model;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Invitation
 *
 * @author Mathieu
 */
class Invitation extends Model{
    
    protected $table="invitation";
    protected $primaryKey="idUser, idGroupe";
    public $timestamps=false;
    
    public function getUser(){
        return $this->hasOne("\model\User","idUser");
    }
    
    public function getGroupe(){
        return $this->hasOne("\model\Groupe","idGroupe");
    }
    
}
