<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace model;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Note
 *
 * @author Mathieu
 */
class Note extends Model{
    
    protected $table="note";
    protected $primaryKey="idLogement, idUser";
    public $timestamps=false;
    
    public function getNoteLog(){
        return $this->belongsTo("\model\Logement","idLogement");
    }
    
    public function getNoteUse(){
        return $this->belongsTo("\model\User", "idUser");
    }
    
}
