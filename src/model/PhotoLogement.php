<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace model;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of PhotoLogement
 *
 * @author Mathieu
 */
class PhotoLogement extends Model{
    
    protected $table="photologement";
    protected $primaryKey="idLogement";
    public $timestamps=false;
    
    public function getPhotos(){
        return $this->belongTo("\model\Logement","idLogement");
    }
    
}
