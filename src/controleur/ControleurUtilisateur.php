<?php
namespace controleur;
use model\User;
use vue\vueUtilisateur;


class ControleurUtilisateur {
	
	public function utilisateurs(){
        $users=User::get();
        (new vueUtilisateur($users->toArray()))->afficher(0);
    }
	
	public function utilisateur($id){
		$user=User::where('idUser','=',$id)->get();
        (new vueUtilisateur($user->toArray()))->afficher(1);
	}
	
}
