<?php
namespace controleur;
use \vue\VueGroupe;
use \model\Groupe;

class ControleurGroupe {
	
	public function groupes(){
		$groupes=Groupe::where('statutGroupe','=','3')->orderBy('idLogement','DESC')->get();
		(new VueGroupe($groupes->toArray()))->afficher(0);
	}
	
	public function groupe($id){
		$groupe=Groupe::where('idGroupe','=',$id)->first();
		(new VueGroupe($groupe->toArray()))->afficher(1);
	}
	
	static function donnerImageLogement($id){
		$logement=Logement::where('idLogement','=',$id)->first();
		return $logement->iconeLogement;
	}
	
	static function donnerProprioGroupe($id){
		$user=User::where('idUser','=',$id);
		return $user->toArray();
	}
	
	static function donnerListeInvite($id){
		$invitation=Invitation::where('idGroupe','=',$id)->get()->toArray();
		$id=0;
		foreach($invitation as $i){
			$users[id]=User::where('idUser','=',$i->idUser)->first();
		}
		return $users;
	}
}