<?php
/**
 * Created by PhpStorm.
 * User: Alexis
 * Date: 09/02/2017
 * Time: 19:26
 */

namespace controleur;
use vue\vueConnexion;
use vue\vueAccueil;
use model\User;

class controleurConnexion
{
    public function connexion(){
        (new vueConnexion())->afficher(0);

    }

    public function inscription(){
        if(isset($_POST['nom']) && isset($_POST['prenom']) &&isset($_POST['email']) &&isset($_POST['pseudo']) &&isset($_POST['password-repeat']) &&isset($_POST['password'])){
            if($_POST['password-repeat']===$_POST['password']){
                $pseudo=filter_var($_POST['pseudo'],FILTER_SANITIZE_STRING);
                $pass=filter_var($_POST['password'],FILTER_SANITIZE_STRING);
                $nom=filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
                $prenom=filter_var($_POST['prenom'],FILTER_SANITIZE_STRING);
                $email=filter_var($_POST['email'],FILTER_SANITIZE_STRING);


                $hash =password_hash($pass, PASSWORD_DEFAULT);
                $u = new User();
                $u->nomUser=$nom;
                $u->prenomUser=$prenom;
                $u->emailUser=$email;
                $u->mdpUser=$hash;
                $u->pseudoUser=$pseudo;
                $u->save();
                (new VueAccueil())->afficher();
                $password='';

            }

        }else{
            (new vueConnexion())->afficher(0);
        }

    }

    public function seConnecter(){
        if(isset($_POST['pseudo']) && isset($_POST['password'])){
            $id=filter_var($_POST['pseudo'],FILTER_SANITIZE_STRING);
            $pass=filter_var($_POST['password'],FILTER_SANITIZE_STRING);
            //$hash =password_hash("Mathieu", PASSWORD_DEFAULT);

            //$g=new Gestionnaire();
            // $g->identifiant='Francis';
            // $g->password=$hash;
            // $g->save();
            $password='';
            try{
                $password=User::select('mdpUser')->where('pseudoUser','=',$id)->first();
                if(password_verify($pass, $password['mdpUser'])){
                    $_SESSION['connecte']='connecter';
                    (new VueAccueil())->afficher();
                }else{
                    (new vueConnexion())->afficher(1);
                    //print "-->".$password."<--";
                }
            }catch(Exception $e){
                (new vueConnexion())->afficher(1);
                //print "-->".$password."<--";
            }
        }else{
            (new vueConnexion())->afficher(0);
        }
    }
}