<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controleur;

use Groupe;

/**
 * Description of FormationGroupe
 *
 * @author Mathieu
 */
class FormationGroupe {

    public function creer() {
        if(isset($_POST['Utilisateur']) && isset($_POST['Texte'])) {
            $util=$_POST['Utilisateur'];
            $text=$_POST['Texte'];
            $groupe = Groupe::get();
            foreach ($groupe as $key) {
                if ($util === $key) {
                    $true = true;
                }
            }
            if (!$true) {
                $ng = new Groupe();
                $ng->statut = 0;
                $ng->url = bin2hex(openssl_random_pseudo_bytes(64, $strong));
                $ng->idUser = $util->id;
                $ng->messageGroupe = $text;
                $ng->save();
            }
        }
    }

    public function afficheDetail() {
        if (isset($_POST['Utilisateur'])) {
            $util = $_POST['Utilisateur'];
            $groupe = Groupe::where("idUser", "=", $util)->get();
            if (!empty($groupe)) {
                $vue = new VueGroupeForm($groupe);
                $vue . render();
            }
        }
    }

    public function ajouterUser() {
        if (isset($_POST['Utilisateur']) && isset($_POST['AjoutUser'])) {
            $idProp = $_POST['Utilisateur'];
            $idAjout = $_POST['AjoutUser'];

            $user = User::where("idUser", "=", $idAjout)->first();
            if (!empty($user)) {
                $invite = new Invitation();
                $invite->idUser = $idAjout;
                $groupe = Groupe::select("idGroupe")->where("idUser", "=", $idProp)->first();
                $invite->idGroupe = $groupe;
                $invite->statutInvitation = 0;
                $invite->save();
            }
        }
    }

    public function affecterLogement() {
        if(isset($_POST['Utilisateur']) && isset($_POST['Logement'])){
            $idUser=$_POST['Utilisateur'];
            $idLog=$_POST['Utilisateur'];
            $groupe = Groupe::where("idUser", "=", $idUser)->first();
            $groupe->idLogement=idLog;
            $groupe->save();
        }
    }

}
