<?php
namespace controleur;
use \model\Logement;
use \vue\VueLogement;

class ControleurLogement {
	
	public function logements(){
		$logements=Logement::where('placesLogement','>',0)->get();
		(new VueLogement($logements->toArray()))->afficher(0);
	}
	
	public function logement($id){
		$logement=Logement::where('idLogement','=',$id)->get();
		(new VueLogement($logement->toArray()))->afficher(1);
	}
}