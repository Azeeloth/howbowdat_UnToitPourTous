<?php

namespace controleur;

use model\Groupe;

function validerGroupe($id){
    $group = Groupe::select('idGroupe', 'statutGroupe')->where('idGroupe','=',$id)->first();

    if($group->statutGroupe == 3){
        $group->statutGroupe = 4;
        $group->save();
    }
}


function refuserGroupe($id){
    $group = Groupe::select('idGroupe', 'statutGroupe')->where('idGroupe','=',$id)->first();

    if($group->statutGroupe == 3){
        $group->statutGroupe = 5;
        $group->save();
    }
}
