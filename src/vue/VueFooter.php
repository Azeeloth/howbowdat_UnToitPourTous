<?php

namespace vue;
/**
 * Created by PhpStorm.
 * User: Alexis
 * Date: 09/02/2017
 * Time: 18:10
 */
class VueFooter {
    public  function getFooter()
    {
		$app=\Slim\Slim::getInstance();
		$url=$app->urlFor('accueil');
        return <<<END
    <footer class="site-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h5>IUT Charlemagne - Team HowbowDat © 2016</h5></div>
                <div class="col-sm-6 social-icons"><a href="#"><i class="fa fa-facebook"></i></a></div>
            </div>
        </div>
    </footer>
    <script src="$url/Bootstrap/assets/js/jquery.min.js"></script>
    <script src="$url/Bootstrap/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="$url/Bootstrap/assets/js/bs-animation.js"></script>
</body>

</html>
END;
    }}