<?php
namespace vue;
use vue\VueHeader;
use vue\VueFooter;

class vueUtilisateur {


	function __construct($tab){
		global $tableau;
		$tableau=$tab;
		global $app;
		$app = \Slim\Slim::getInstance();
	}

	private function users(){
		global $app;
		global $tableau;
		$url=$app->urlFor('accueil');
		$html="";
		$html.="";
		
		foreach($tableau as $user){
			$html.= <<<END
			<div class="containerListe">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-2">
						<div class="productbox">
							<center><img class="img-membre" src=$url/img/user/$user[imageUser].jpg></center>
							<div class="membretitle"> $user[pseudoUser] </div>
							<div class="bouton">
								<div class="pull-right">
									<a class="btn btn-success btm-sm" href="$url/membres/$user[idUser]" role="button">Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
END;
		}
		return $html;
	}
	

	private function user(){
		global $app;
		global $tableau;
		$url=$app->urlFor('accueil');
		$user=$tableau[0];
		$html=<<<END
		<div class="boutonRetour">
			<div class="pull-right">
				<a class="btn btn-success btm-sm" href="$url/membres" role="button">Retour</a>
			</div>
		</div>
		<center>
		<div class="container2">
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-2">
					<div class="productbox">
						<center><img class="img-membre" src=$url/img/user/$user[imageUser].jpg></center>
						<div class="membretitle"> $user[pseudoUser] </div>
					</div>
				</div>
			</div>
		</div>
		</center>
		
		<center><div class=description>
		<p> $user[nomUser] $user[prenomUser] - $user[emailUser] - $user[messageUser] </p>
		</div>
		</center>
		
END;
		return $html;
	}
	
	public function afficher($select){
	    echo (new VueHeader())->getHeader();
		switch($select){
			case 0:
				echo $this->users();
				break;
			case 1:
				echo $this->user();
				break;
		}
		echo (new VueFooter())->getFooter();
	}
}
