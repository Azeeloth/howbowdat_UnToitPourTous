<?php
namespace vue;

class VueGroupe {
	
	public $httpRequest;

    function __construct($tab){
		global $tableau;
		$tableau=$tab;
		global $app;
		$app=\Slim\Slim::getInstance();
	}
	
	private function entete($css){
			global $app;
			//$url=$app->urlFor('accueil');
			$url='https://webetu.iutnc.univ-lorraine.fr/www/chaffaut1u/CrazyCharlyDay/howbowdat_UnToitPourTous/';
			//<link rel="stylesheet" href="$url/web/css/$css">
			
			$html=<<<END
			<!doctype html>
			<html lang="fr">
			<head>
				<meta charset="UTF-8">
				<title>Collocation pour Tous</title>
				
			</head>

				
			
END;
		return $html;
	}
	
	private function groupesComplets(){
		global $tableau;
		global $app;
		$url=$app->urlFor('accueil');
		$html=$this->entete(null)."<body>";
		foreach($tableau as $groupe){
			$id=ControleurGroupe::donnerImageLogement($groupe[idLogement]);
			$html.=<<<END
			<section class="groupesComplets">
			<p> <a href=$url/groupes/$groupe[idGroupe]>$groupe[idGroupe]</a> <img src=$url/img/apart/$id.jpg style=width:50px;height=50px;> </p>
			</section>
			<br>
END;
		}
		$html.='</body></html>';
	}
	
	private function groupe(){
		global $tableau;
		global $app;
		$url=$app->urlFor('accueil');
		$html="<body>";
		$groupe=$tableau[0];
		if(isset($groupe)){
			$prop=ControleurGroupe::donnerProprioGroupe();
			$html.=<<<END
			$prop[pseudoUser]  $groupe[urlGroupe]  $groupe[messageGroupe]
END;
			
			$usersList=ControleurGroupe::donnerListeInvite($groupe->idGroupe);
			foreach($usersList as $user){
				$html.=<<<END
				<div class="containerListe">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-2">
						<div class="productbox">
							<center><img class="img-membre" src=$url/img/user/$user[imageUser].jpg></center>
							<div class="membretitle"> $user[pseudoUser] </div>
							<div class="bouton">
								<div class="pull-right">
									<a class="btn btn-success btm-sm" href="$url/membres/$user[idUser]" role="button">Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
END;
			}
			return $html;
		}else{
			return "error";
		}
	}
	
	public function afficher($select){
		echo (new VueHeader())->getHeader();
		switch($select){
			case 0:
				echo $this->groupesComplets();
				break;
			case 1:
				echo $this->groupe();
				break;
		}
		echo (new VueFooter())->getFooter();
	}
}