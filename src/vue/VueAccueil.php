<?php

namespace vue;

use vue\VueHeader;
use vue\VueFooter;

class VueAccueil{
	
	public function afficher(){
        echo (new VueHeader())->getHeader().'
    <div class="jumbotron hero">
        <div class="container">
            <div class="col-md-6 col-md-offset-6 col-md-pull-3 get-it">
                <h1 class="text-center">La Coloc en ligne !</h1>
                <p class="text-center">Logements de qualités, votre satisfaction est notre priorité.</p>
                <p class="text-center"></p>
            </div>
        </div>
    </div>

    <div class="main-content">
		<section class="testimonials">
			<h2 class="text-center">Vous cherchez un logement en colocation ?</h2>
			<blockquote>
				<p>La Coloc en ligne vous propose d\'une part des logements et d\'autre part une liste de membres également à la recherche d\'une colocation.</p>
				<footer>Team HowbowDat</footer>
			</blockquote>
		</section>
	   <div class="register-photo" id="inscription">
			<div class="form-container">
				<div class="image-holder"></div>
				<form method="post" action = "/inscription">
					<h2 class="text-center">Vous n\'êtes pas encore <strong>inscrits ?</strong> <br /><br /> Profitez en !</h2>

					 <div class="form-group">
						<input class="form-control" type="text" name="nom" placeholder="Nom">
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="prenom" placeholder="Prénom">
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="email" placeholder="Adresse mail">
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="pseudo" placeholder="Pseudonyme">
					</div>
					<div class="form-group">
						<input class="form-control" type="password" name="password" placeholder="Mot de passe">
					</div>
					<div class="form-group">
						<input class="form-control" type="password" name="password-repeat" placeholder="Répéter mot de passe">
					</div>
					<div class="form-group">
						<button class="btn btn-info btn-block" type="submit">S\'inscrire</button>
					</div><a href="#" class="already">Vous avez déja un compte ? Connectez vous ici.</a>
				</form>
			</div>
		</div>
    </div>

'.(new VueFooter())->getFooter();
    }
    
}
