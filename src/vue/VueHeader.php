<?php

namespace vue;
/**
 * Created by PhpStorm.
 * User: Alexis
 * Date: 09/02/2017
 * Time: 18:10
 */
class VueHeader{
  public  function getHeader(){
	  $app=\Slim\Slim::getInstance();
	  $url=$app->urlFor('accueil');
        return <<<END
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>La Coloc en ligne !</title>
    <link rel="stylesheet" href="$url/Bootstrap/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic">
    <link rel="stylesheet" href="$url/Bootstrap/assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="$url/Bootstrap/assets/css/user.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="$url/Bootstrap/assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="$url/Bootstrap/assets/css/Registration-Form-with-Photo.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header"><a class="navbar-brand navbar-link" href="$url"><i class="glyphicon glyphicon-bed"></i>La Coloc en ligne !</a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active" role="presentation"><a href="$url">Accueil </a></li>
                    <li role="presentation"><a href="$url/membres">Membres </a></li>
                    <li role="presentation"><a href="$url/logements">Logements </a></li>
                    <li role="presentation"><a class="text-uppercase bg-info" href="/connexion" data-bs-hover-animate="tada"><strong>Connexion</strong> </a></li>
                    <li role="presentation"><a class="text-uppercase bg-info" href="/#inscription" data-bs-hover-animate="tada"><strong>Inscription </strong></a></li>

                </ul>
            </div>
        </div>
    </nav>

END;

}
}
