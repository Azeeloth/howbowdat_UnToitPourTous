<?php

namespace vue;
use vue\VueHeader;
use vue\VueFooter;

class VueLogement {

	function __construct($tab){
		global $tableau;
		$tableau=$tab;
		global $app;
		$app = \Slim\Slim::getInstance();
	}
	
	public function logements(){
		global $app;
		global $tableau;
		$url=$app->urlFor('accueil');
		$html="<body>";
		foreach ($tableau as $l){
			$html.=<<<END
			<div class="containerListe">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-2">
						<div class="productbox">
							<center><img class="img-membre" src=$url/img/apart/$l[iconeLogement].jpg></center>
							<div class="membretitle"> Places : $l[placesLogement] </div>
							<div class="bouton">
								<div class="pull-right">
									<a class="btn btn-success btm-sm" href="$url/logements/$l[idLogement]">Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
END;
		}
		$html.="</body>";
		return $html;
	}
	
	public function logement(){
		global $app;
		global $tableau;	
		$url=$app->urlFor('accueil');
		$loge=$tableau[0];
		$html=<<<END
		
		<div class="boutonRetour">
			<div class="pull-right">
				<a class="btn btn-success btm-sm" href="$url/logements" role="button">Retour</a>
			</div>
		</div>
		<center>
		<div class="container2">
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-2">
					<div class="productbox">
					<center><img class="img-membre" src=$url/img/apart/$loge[iconeLogement].jpg></center>
						<div class="membretitle">  Places : $loge[placesLogement] </div>
					</div>
				</div>
			</div>
		</div>
		</center>
		
		
		
END;
		return $html;
	}
	
	public function afficher($select){
        echo (new VueHeader())->getHeader();
		switch($select){
			case 0:
				echo $this->logements();
				break;
			case 1:
				echo $this->logement();
				break;
		}
        echo (new VueFooter())->getFooter();
	}
	
	
}