<?php
/**
 * Created by PhpStorm.
 * User: Alexis
 * Date: 09/02/2017
 * Time: 19:27
 */

namespace vue;


class vueConnexion
{

    public function afficher($select){
        echo (new VueHeader())->getHeader();
        switch($select){
            case 0:
                echo '


    <div class="main-content">

	   <div class="register-photo">
			<div class="form-container">
				<div class="image-holder"></div>
				<form id="connecter" action ="/connect" method="post" >
					<h2 class="text-center">Connexion <br /><br /> Profitez en !</h2>
<div class="form-group">
						<input class="form-control" type="text" name="pseudo" placeholder="Pseudonyme">
					</div>
					<div class="form-group">
						<input class="form-control" type="password" name="password" placeholder="Mot de passe">
					</div>
					<div class="form-group">
						<button class="btn btn-info btn-block" type="submit">S\'inscrire</button>
					</div>
				</form>
			</div>
		</div>
    </div>

';
                break;
            case 1:
                echo '


    <div class="main-content">

	   <div class="register-photo">
			<div class="form-container">
				<div class="image-holder"></div>
				<form id="connecter" action ="/connect" method="post" >
					<h2 class="text-center">Connexion <br /><br /> MOT DE PASSE OU LOGIN INCORRECT</h2>
<div class="form-group">
						<input class="form-control" type="text" name="pseudo" placeholder="Pseudonyme">
					</div>
					<div class="form-group">
						<input class="form-control" type="password" name="password" placeholder="Mot de passe">
					</div>
					<div class="form-group">
						<button class="btn btn-info btn-block" type="submit">S\'inscrire</button>
					</div>
				</form>
			</div>
		</div>
    </div>

';
                break;
        }
        echo (new VueFooter())->getFooter();
    }

}